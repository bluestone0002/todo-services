const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const Todo = require('./Todo');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 2002;

app.use(cors());
app.use(express.json());

const uri = "mongodb://mongo:27017/todotodos";
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true  }
);
const connection = mongoose.connection;
connection.once('open', () => {
  console.log("MongoDB database connection established successfully");
})

app.get('/', (req, res) => {
  Todo.find()
    .then(todos => res.json(todos))
    .catch(err => res.status(400).json('Error: ' + err));
});

app.post('/add', (req, res) => {
  const username = req.body.username;
  const description = req.body.description;

  const newTodo = new Todo({
    username,
    description,
  });

  newTodo.save()
  .then(() => res.json('Todo added!'))
  .catch(err => res.status(400).json('Error: ' + err));
});

app.get('/:id', (req, res) => {
  Todo.findById(req.params.id)
    .then(todo => res.json(todo))
    .catch(err => res.status(400).json('Error: ' + err));
});


app.delete('/:id', (req, res) => {
  Todo.findByIdAndDelete(req.params.id)
    .then(() => res.json('Todo deleted.'))
    .catch(err => res.status(400).json('Error: ' + err));
});

app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});