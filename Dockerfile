FROM node:10
WORKDIR /usr/src/todoservice
COPY node_modules .
RUN npm install
COPY . .
EXPOSE 2002
CMD ["node", "todos.js"]